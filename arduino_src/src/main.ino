/* ==========================================================================
   LIBRARY USED
   ========================================================================== */
#include <Ethernet.h>
#include <SPI.h>
#include <Stdio.h>
#include <utility/w5100.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>

/* ==========================================================================
   FUNCTIONS PROTOTYPES
   ========================================================================== */
bool byte_array_contains(const byte, unsigned int, const byte, unsigned int length);

/* ==========================================================================
   DEFINED, CONSTANTS AND GLOBALS
   ========================================================================== */

// Version and project name
#define PROJECT_NAME "LLDP discovery"
#define PROJECT_VERSION "v0.0.0"

// Debug to console (uncomment to enable debugging to the serial console)
// #define DEBUG_CONSOLE

// Debug message handling
#ifdef DEBUG_CONSOLE
  #define DEBUG_PRINTLN(x)     Serial.println(x)
  #define DEBUG_PRINT(x)       Serial.print(x)
  #define DEBUG_PRINTLN_DEC(x) Serial.println(x, DEC)
#else
  #define DEBUG_PRINTLN(x)
  #define DEBUG_PRINT(x)
  #define DEBUG_PRINTLN_DEC(x)
#endif

// Define LCD address
#define LCD_ADDRESS 0x27

// The socket that will be openend in RAW mode
SOCKET s;

// Receive buffer
byte rbuf[380 + 14];

// Length of data to receive
int rbuflen;

// TLV type and length
char TLVTYPE[3];
char TLVLENGTH[3];

// Dst mac for lldp traffic, we look for this
byte lldp_mac[] = {0x01, 0x80, 0xc2, 0x00, 0x00, 0x0e};

/* ==========================================================================
   HARDWARE INITIALIZATION
   ========================================================================== */

// Initialize the LCD module
LiquidCrystal_I2C lcd(LCD_ADDRESS,2,1,0,4,5,6,7);


/* ==========================================================================
   SETUP
   ========================================================================== */
void setup() {
  // Console serial port for debugging
  Serial.begin(9600);

  // LCD initialization
  lcd.begin (16,2);
  lcd.setBacklightPin(3,POSITIVE);
  lcd.setBacklight(HIGH);

  // Display our banner
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print((const String)PROJECT_NAME);
  lcd.setCursor(0, 1);
  lcd.print((const String)PROJECT_VERSION);

  // Ethernet initialization
  W5100.init();
  W5100.writeSnMR(s, SnMR::MACRAW);
  W5100.execCmdSn(s, Sock_OPEN);

  // Delay to let the time to show our banner
  delay(1500);

  // Display that we are waiting for an LLDP frame
  lcd.clear();
  lcd.print("Waiting for LLDP");
}

/* ==========================================================================
   LOOP
   ========================================================================== */
void loop() {
  rbuflen = W5100.getRXReceivedSize(s);
  if (rbuflen > 0) {
    if (rbuflen > sizeof(rbuf)) {
      rbuflen = sizeof(rbuf);
    }
    W5100.recv_data_processing(s, rbuf, rbuflen);
    W5100.execCmdSn(s, Sock_RECV);
    lldpcheck();
  }
}

/* ==========================================================================
   PUBLIC FUNCTIONS
   ========================================================================== */

/* --------------------------------------------------------------------------
 lldpcheck
 --------------------------------------------------------------------------
 TODO

 Params: None

 Return: None
 -------------------------------------------------------------------------- */
void lldpcheck() {
  unsigned int rbufIndex = 2;
  char tmp[rbuflen * 2 + 1];
  byte first;
  byte second;

  if (byte_array_contains(rbuf, rbufIndex, lldp_mac, sizeof(lldp_mac))) {

    DEBUG_PRINT(F("Received "));
    DEBUG_PRINTLN_DEC(rbuflen);

    if (rbuflen == 0) {
      return;
    }

    for (int i = 0; i < rbuflen; i++) {
      first = (rbuf[i] >> 4) & 0x0f;
      second = rbuf[i] & 0x0f;
      tmp[i * 2] = first + 48;
      tmp[i * 2 + 1] = second + 48;
      if (first > 9) {
        tmp[i * 2] += 39;
      }
      if (second > 9) {
        tmp[i * 2 + 1] += 39;
      }
    }
    tmp[rbuflen * 2] = 0;
    DEBUG_PRINTLN(tmp);
    DEBUG_PRINTLN("Data ");

    // main while loop for data extraction
    int y = 0;
    int x = 32; // start reading at the first TLV value so we start at 32.  We
                // don't care about the source/dest mac.
    while (y < 10) {
      // find TLV Data Type
      for (int i = 0; i < 2; i++) {
        TLVTYPE[i] = tmp[x++];
      }

      TLVTYPE[3] = '\0'; // required null pointer set

      // find TLV lenghts
      for (int i = 0; i < 2; i++) {
        TLVLENGTH[i] = tmp[x++];
      }

      // this converts the Hex like value in TLVLENGTH to an int (xx) so we can
      // use it for math
      unsigned int xx = 0;
      sscanf(TLVLENGTH, "%x %x", &xx);
      DEBUG_PRINT("TLVLENGHT as int ");
      DEBUG_PRINTLN(xx);

      // 2x for hex like values for spaces to move
      xx = xx * 2;
      DEBUG_PRINT("TLVL times 2 is: ");
      DEBUG_PRINTLN(xx);

      if (xx < 100) { // skips overly large characters that we don't have enough
                      // memory for
        char TLVDATA[xx + 1];

        for (int i = 0; i < xx; i++) {
          TLVDATA[i] = tmp[x++];
        }
        TLVDATA[xx] = '\0';
        DEBUG_PRINTLN("TLVDATA char");
        DEBUG_PRINTLN(TLVDATA);

        // convert TLVTYPE TO INT FOR PROCESSING
        int xx1;
        sscanf(TLVTYPE, "%x %x", &xx1);
        DEBUG_PRINTLN("TLVTYPE IN INT format");
        DEBUG_PRINTLN(xx1);

        if (xx1 == 0) { // end of packet!
          y = 20; // exit the loop.  This should look for the end of sequence in
                  // the datastream, but we just count to 20 for now
          DEBUG_PRINTLN("End at xx1 == 0");
          return;
        }
        DEBUG_PRINTLN("After xx1 == 0");

        if (xx1 == 254) {   // vlan id
          DEBUG_PRINTLN("Got VLAN ID");
          int vlanhold = 9; // start at 9
          char vlan[5];
          for (int i = 0; i < 4; i++) {
            vlan[i] = TLVDATA[vlanhold];
            vlanhold++;
          }
          vlan[5] = '\0'; // null pointer
          DEBUG_PRINTLN("VLAN char:");
          DEBUG_PRINTLN(vlan);

          int vlanxx;
          sscanf(vlan, "%x %x", &vlanxx);
          if ((vlanxx > 0) && (vlanxx < 3000)) {
            lcd.setCursor(0, 0);
            lcd.print(vlanxx);
          }
        }

        if (((xx1 == 4) || (xx1 == 10) || (xx1 == 2066))) { // ascii conversation matches
          DEBUG_PRINTLN("ascii conversation matches");
          // This pulls the data apart and converts it back into ASCII
          // We have to take the char bytes out, add them together in a char
          // array, treat that like a hex, then find the ascii value
          int zz = 0;         // main loop for ASCII counter
          int varhold = 0;    // holder for each word
          char asciiname[xx]; // final placeholder for ASCII name
          // char hexconverstion[3]; //holder to combine the values together to
          // make the HEX
          char tempchar; // another holders
          char *hexconverstion = (char *)malloc(3);

          while (zz < xx / 2) {
            for (int i = 0; i < 2; i++) {
              hexconverstion[i] = TLVDATA[varhold];

              DEBUG_PRINTLN("###START OF ASCII WHILE LOOP####");
              DEBUG_PRINTLN("I value is:");
              DEBUG_PRINTLN(i);
              DEBUG_PRINTLN("zz value is:");
              DEBUG_PRINTLN(zz);
              DEBUG_PRINTLN("TLVDATA is is:");
              DEBUG_PRINTLN(TLVDATA[varhold]);
              DEBUG_PRINTLN("###END OF ASCII WHILE LOOP####");

              varhold++;
              hexconverstion[3] = '\0'; // null pointer
            }
            hexconverstion[3] = '\0'; // null pointer

            int xx2;
            sscanf(hexconverstion, "%x %x", &xx2);
            // convert to char
            tempchar = (char)xx2;

            if (xx1 == 4) {
              // Interface name mathc a display fix
              asciiname[zz - 1] = tempchar; // junk byte in
            }
            else {
              // normal switch name
              asciiname[zz] = tempchar;
            }

            free(hexconverstion);
            zz++;
          }

          if (xx1 == 4) {
            // Interface name mathc a display fix
            asciiname[zz - 1] = '\0';
          }
          else {
            // normal switch name
            asciiname[zz] = '\0';
          }

          // put the interface and switch name in the right place
          if (xx1 == 4) {
            // this is the interface name

            lcd.clear();
            lcd.setCursor(6, 0);
            lcd.print(asciiname);
            DEBUG_PRINT("asciiname:");
            DEBUG_PRINTLN(asciiname);
          }
          else {
            // this is the switchport name
            lcd.setCursor(0, 1);
            lcd.print(asciiname);
            DEBUG_PRINT("asciiname1:");
            DEBUG_PRINTLN(asciiname);
          }
        }
      }
      else {
        x = x + xx;
      }

      y++;
    }
  }
}

/* --------------------------------------------------------------------------
 byte_array_contains
 --------------------------------------------------------------------------
 TODO

 Params: TODO

 Return: TODO
 -------------------------------------------------------------------------- */
// stolen from CDP sniffer code.  This triggers on the correct mac address
bool byte_array_contains(const byte a[], unsigned int offset, const byte b[],unsigned int length) {
  for (unsigned int i = offset, j = 0; j < length; ++i, ++j) {
    if (a[i] != b[j])
      return false;
  }

  return true;
}
